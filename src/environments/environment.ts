// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConf: {
    apiKey: "AIzaSyDUVh9AuFW2F58h1o_2RVRBlPpgyLy2TBE",
    authDomain: "test-angular-69cec.firebaseapp.com",
    projectId: "test-angular-69cec",
    storageBucket: "test-angular-69cec.appspot.com",
    messagingSenderId: "171495801965",
    appId: "1:171495801965:web:2e692550b2b2f03d7fab64"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
