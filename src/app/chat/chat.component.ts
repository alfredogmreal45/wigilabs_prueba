import { Component, OnInit } from '@angular/core';
import { FirestoreService } from '../services/firestore/firestore.service';
import { FormControl, Validators } from '@angular/forms';
@Component({
  selector: 'chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  disabled: boolean = false
  messages: any[] = []
  message = new FormControl('', [Validators.required])
  constructor(private firestore: FirestoreService) { }

  ngOnInit(): void {
    this.firestore.getMessages().subscribe(res => {
      this.messages = res
    })
  }
  generateLetter(){
    let letras: any[] = ["a","b","c","d","e","f","0","1","2","3","4","5","6","7","8","9"];
    let numero: any = (Math.random()*15).toFixed(0);
    return letras[numero];
  }
  colorHEX(){
    var coolor = "";
    for(var i=0;i<6;i++){
      coolor = coolor + this.generateLetter() ;
    }
    return "#" + coolor;
  }
  makeRequestMessage (value: any, hour: any, minute: any, id: any, date: any, colorMessage: any) {
    this.firestore.sendMessage({
      content: value,
      time: `${hour.substr(-2)}:${minute.substr(-2)}`,
      created_at: date,
      userId: id,
      colorMessage: colorMessage
    }).then(res => {
      this.disabled = false
    })
  }
  sendMessage () {
    const date = new Date()
    let session: any = localStorage.getItem('session')
    let hour = '0' + date.getHours()
    let minute = '0' + date.getMinutes()
    let id: any = Date.now()
    let colorMessage: any = this.colorHEX()
    if (this.message.status.toLowerCase() !== 'invalid' && !this.disabled) {
      this.disabled = true
      // Creo el usuario nuevo que esta ingresando sino traigo su session
      if (session === null) {
        this.firestore.createUser({
          id: ''+id,
          colorMessage: colorMessage
        })
        localStorage.setItem('session', JSON.stringify({id: id}))
        this.makeRequestMessage(this.message.value, hour, minute, id, date, colorMessage)
        this.message.reset()
      } else {
        let user: any = JSON.parse(session)
        this.firestore.getUser(user.id).subscribe(resUser => {
          const users: any[] = resUser
          if (resUser.length) {
            this.makeRequestMessage(this.message.value, hour, minute, users[0].id, date, users[0].colorMessage)
            this.message.reset()
          }
        })
      }
    }
  }

}
