import { Component, OnInit, AfterViewInit} from '@angular/core';
import { FirestoreService } from '../services/firestore/firestore.service';
import { Loader } from "@googlemaps/js-api-loader"
@Component({
  selector: 'map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit, AfterViewInit {
  constructor(public firestore: FirestoreService) { }

  ngOnInit(): void {
    const loader = new Loader({
      apiKey: "AIzaSyBHPtJ_13sra3bc-7bbBN3pmn3O_qVVRO0",
    });
    loader.load().then(() => {
      const infoWindow = new google.maps.InfoWindow({
        content: "",
        disableAutoPan: true,
      });
      // Obtengo las tareas
      this.firestore.getTasks().subscribe(res => {
        const tasks: Array<any> = res.map(item => item)
        if (tasks.length) {
          const firstTask = { lat: JSON.parse(tasks[0].latitude), lng: JSON.parse(tasks[0].longitude) }
          const map = new google.maps.Map(document.getElementById("map") as HTMLElement, {
            center: firstTask,
            zoom: 8,
          });
          tasks.forEach(task => {
            const marker = new google.maps.Marker({
              position: { lat: JSON.parse(task.latitude), lng: JSON.parse(task.longitude) },
              map: map,
            });
            const label = `Tarea: ${task.title}, Latitud: ${task.latitude} º, Longitud: ${task.longitude} º`
            // markers can only be keyboard focusable when they have click listeners
            // open info window when marker is clicked
            marker.addListener("click", () => {
              infoWindow.setContent(label);
              infoWindow.open(map, marker);
            });
          })
        }
        const uluru = { lat: -25.344, lng: 131.036 }
      })
    });
  }
  ngAfterViewInit () {}

}
