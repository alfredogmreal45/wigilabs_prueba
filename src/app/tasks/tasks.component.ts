import { Component, OnInit } from '@angular/core';
import { FirestoreService } from '../services/firestore/firestore.service';
import {FormGroup, FormControl, Validators} from '@angular/forms';
@Component({
  selector: 'tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {
  form: any
  tasks: Array<any> = []
  constructor(public firestore: FirestoreService) {
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      /**
       * Nombre de la tarea
       */
      title: new FormControl('', [Validators.required]),
      /**
       * Latitud
       */
      latitude: new FormControl('', [Validators.required]),
      /**
       * Longitud
       */
      longitude: new FormControl('', [Validators.required])
    })
    this.firestore.getTasks().subscribe(res => {
      this.tasks = res
    })
  }
  deleteTask (id: string) {
    this.firestore.deleteTask(id)
  }
  updateTask (task: any) {
    this.firestore.updateTask({isDone: !task.isDone}, task.id )
  }
  createTask () {
    this.form.markAllAsTouched()
    if (this.form.status.toLowerCase() !== 'invalid') {
      this.firestore.createTask({...this.form.value, isDone: false, created_at: new Date()})
      this.form.reset()
    }
  }
  getTitleError () {
    return this.title.errors.required && 'La tarea es requerida' 
  }
  getLongitudeError () {
    return this.longitude.errors.required && 'La longitud es requerida'
  }
  getLatitudeError () {
    return this.latitude.errors.required && 'La latitud es requerida'
  }
  /**
   * Getters para las propiedades del formulario
   */
  get title () {
    return this.form.get('title')
  }
  get longitude () {
    return this.form.get('longitude')
  }
  get latitude () {
    return this.form.get('latitude')
  } 
}
