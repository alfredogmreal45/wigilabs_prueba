import { Component, OnInit } from '@angular/core';
interface Option {
  id: string,
  name: string
}
@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  currentOption : string = ''
  options: Array<Option> = [
    {id: '', name: 'Galería'},
    {id: 'tasks', name: 'Tareas'},
    {id: 'map-section', name: 'Mapa'},
    {id: 'chat', name: 'Chat'},
    {id: 'contact', name: 'Contacto'}
  ]
  constructor() { }

  ngOnInit(): void {
  }

}