import { ThrowStmt } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/compat/firestore';
@Injectable({
  providedIn: 'root'
})
export class FirestoreService {
  constructor(private firestore: AngularFirestore) {
  }
  createContact (contact: any) {
    return this.firestore.collection('contacts').add(contact)
  }
  sendMessage (message: any) {
    return this.firestore.collection('messages').add(message)
  }
  createUser (user: any) {
    this.firestore.collection('users').add(user)
  }
  getUser (id: string) {
    return this.firestore.collection('users', ref => ref.where('id', '==', ''+id)).valueChanges()
  }
  getMessages () {
    return this.firestore.collection('messages', ref => ref.orderBy('created_at', 'asc')).valueChanges()
  }
  getTasks () {
    return this.firestore.collection('tasks', ref => ref.orderBy('created_at', 'asc')).valueChanges({idField: 'id'})
  }
  createTask (task: any) {
    return this.firestore.collection('tasks').add(task)
  }
  updateTask (task: any, id: string): void {
    this.firestore.doc(`tasks/${id}`).update(task)
  }
  deleteTask(id: string): void {
    this.firestore.doc(`tasks/${id}`).delete()
  }
}
