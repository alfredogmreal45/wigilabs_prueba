import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import { FirestoreService } from '../services/firestore/firestore.service';

@Component({
  selector: 'contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.css']
})
export class ContactFormComponent implements OnInit {
  form: any
  success: boolean = false
  constructor(public firestore: FirestoreService) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      /**
       * Email
       */
      email: new FormControl('', [Validators.required]),
      /**
       * Comentario
       */
      comment: new FormControl('', [Validators.required]),
    })
  }
  sendComment () {
    this.form.markAllAsTouched()
    if (this.form.status.toLowerCase() !== 'invalid') {
      this.firestore.createContact(this.form.value).then(res => {
        this.form.reset()
        this.success = true
        setTimeout(() => {
          this.success = false
        }, 3000)
      })
    }
  }
  getEmailError () {
    if (this.email.errors.email) {
      return 'El correo no es valido'
    }
    return this.email.errors.required && 'El correo es requerido'
  }
  getCommentError () {
    return this.comment.errors.required && 'El comentario es requerido'
  }
  get email () {
    return this.form.get('email')
  }
  get comment () {
    return this.form.get('comment')
  }
}
